-- SUMMARY --

The language domain token creates additional site tokes for language domains.
One for the current language and one for every single defined language.

For a full description of the module, visit the project page:
  http://drupal.org/project/language_domain_token

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/language_domain_token


-- REQUIREMENTS --

Drupal 7


-- INSTALLATION --

* Install as usual.

* Setup your language domains ... if you haven't done that already.


-- CONTACT --

Current maintainers:
* Fredi Bach (Subby) - http://drupal.org/user/2738721

This project has been sponsored by:
* getunik
  Specialized in NGO clients. Located in Zurich, Switzerland.